// Based on https://gist.github.com/AshikNesin/e44b1950f6a24cfcd85330ffc1713513
import React from 'react';
import {post} from 'axios';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import LinearProgress from '@material-ui/core/LinearProgress';
import {styles} from './styles';


class EstimateGraphForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      errorMessage: null,
      graphDimention: 4000,
      speedAccuracyTradeOff: 1,
      isLoading: false,
      loadingStatus: null
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onFileChange = this.onFileChange.bind(this);
    this.sendGraph = this.sendGraph.bind(this);
    this.onGraphDimentionChange = this.onGraphDimentionChange.bind(this);
    this.onSpeedAccuracyTradeOff = this.onSpeedAccuracyTradeOff.bind(this);
    this.isValidForSend = this.isValidForSend.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.setState({
      isLoading: true,
      loadingStatus: "wait to recieve the gcc"
    });
    this.sendGraph(this.networkData)
      .then((response) => {
        this.props.OnResponse({...response.data, networkData: this.networkData});
        this.setState({isLoading: false, errorMessage: null});
      })
      .catch((error) => {
        this.props.OnError(error.response);
        this.setState({isLoading: false, errorMessage: "problem with get the gcc"});
      });
  }

  async onFileChange(e) {
    const file = e.target.files[0];

    this.setState({
      isLoading: true,
      loadingStatus: "Reads the file and checks it's validity"
    });
    try {
      this.networkText = await this.extractNetworkDataFromFile(file);
      this.networkData = await this.checkAndExtractNetworkDataFromFile(this.networkText);
      this.setState({file, fileError: null});
    }
    catch(error) {
      this.setState({file: null, fileError: error});
    } finally {
      this.setState({isLoading: false});
    }
  }

  extractNetworkDataFromFile(file) {
    return new Promise((resolve, reject) => {
      const onError = (errorMessage) => {
        this.setState({
          fileError: errorMessage
        });
        console.error(errorMessage);
        reject(errorMessage);
      };
      const reader = new FileReader();
      reader.addEventListener("load", (event) => {
        const text = event.target.result;
        resolve(text);
      });

      reader.addEventListener("error", (error) => {
        onError("can't read the file");
      });

      reader.readAsText(file);
    });

  }
  
  checkAndExtractNetworkDataFromFile(text) {
    return new Promise((resolve, reject) => {
      const onError = (errorMessage) => {
        this.setState({
          fileError: errorMessage
        });
        console.error(errorMessage);
        reject(errorMessage);
      };
      if(!text || text === "") {
        onError("The file is empty");
        return;
      }
      const pairs = text.split(",");
      if(pairs.length < 2) {
        onError("each edge in the file must be seperated by new line");
        return;
      }

      const edgesData = [];
      for(const pair of pairs) {
        if(pair === "") {
          continue;
        }
        const pairArray = pair.split(" ");
        if(pairArray.length !== 2 || pairArray.some(item => isNaN(item))) {
          onError("each edge must contain two numbers");
          return;
        }

        edgesData.push(pairArray.map(item => Number(item)));
      }
      resolve(edgesData);
    });

  }

  onGraphDimentionChange(e) {
    this.setState({graphDimention: e.target.value});
  }

  onSpeedAccuracyTradeOff(e) {
    this.setState({speedAccuracyTradeOff: e.target.value});
  }

  sendGraph() {
    const url = `https://gentle-reaches-72166.herokuapp.com/estimate/gcc`;
    const request = {
      "n": parseInt(this.state.graphDimention),
      "k": this.state.speedAccuracyTradeOff,
      "graph": this.networkText
    }
    return post(url, request);
  }

  isValidForSend() {
    return !!this.state.file &&
      !isNaN(this.state.graphDimention) &&
      !isNaN(this.state.speedAccuracyTradeOff);
  }

  render() {
    const {classes} = this.props;
    return (
      <Card className={classes.root} >
        <CardContent>
          <form onSubmit={this.onFormSubmit}>
            <h1>Estimate graph</h1>
            {this.state.isLoading &&
              <div>
                <span>{this.state.loadingStatus}...</span>
                <LinearProgress />
              </div>}
            {this.state.errorMessage && <div style={{color: "red"}}>{this.state.errorMessage}</div>}
            <div>
              <label htmlFor="filePicker">Choose network edges file: <br /> 
              (The file must include each edge in seperate line. Each edge must be two vertex numbers seperated by space)
              </label>
            </div>
            <input id="filePicker" type="file" onChange={this.onFileChange} />

            <TextField
              onChange={this.onGraphDimentionChange}
              defaultValue={this.state.graphDimention}
              id="Graph-dimention"
              label="Graph dimention"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              size="small"
              fullWidth
              margin="dense"
            />
            <TextField
              onChange={this.onSpeedAccuracyTradeOff}
              defaultValue={this.state.speedAccuracyTradeOff}
              id="Speed-Accuracy-Trade-Off"
              label="Speed Accuracy Trade Off"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              size="small"
              fullWidth
              margin="dense"
            />

            <Button type="submit" variant="contained" color="primary" disabled={this.state.isLoading || !this.isValidForSend()}>Upload</Button>

          </form>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(EstimateGraphForm);