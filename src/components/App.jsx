import React from 'react';
import '../App.css';
import EstimateGraphForm from './EstimateGraphForm';
import VisualGraph from "./VisualGraph";
import EstimatorResults from './EstimatorResults';
import ColorLegend from './ColorLegend';
import Card from '@material-ui/core/card';
import * as d3 from "d3";



class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      estimatedGcc: "Upload graph first",
      runTimeMs: 0,
      edgesData: null,
      color: null
    };
  }

  colorScale = d3.scaleSequential([0, 1], d3.interpolateTurbo);

  setNetworkEdgesData(edgesData) {
    this.setState({edgesData});
  }

  OnEstimateResponse(responseData) {
    this.setState({
      estimatedGcc: responseData.gcc,
      runTimeMs: responseData.calculationTimeMs,
      edgesData: responseData.networkData,
      color: this.colorScale(responseData.gcc)
    });

  }

  OnEstimateError(error) {
    this.setState({
      estimatedGcc: "Upload graph first",
      runTimeMs: 0,
      edgesData: null,
      color: null
    });

    console.error(error);
  }

  render() {
    return (
      <div className="App">
        <EstimateGraphForm
          OnResponse={(r) => this.OnEstimateResponse(r)}
          OnError={(r) => this.OnEstimateError(r)}

        />
        <EstimatorResults
          estimatedGcc={this.state.estimatedGcc}
          runTimeMs={this.state.runTimeMs}></EstimatorResults>
        {this.state.edgesData && <Card>

          <VisualGraph data={this.state.edgesData} color={this.state.color}></VisualGraph>
          <ColorLegend color={this.colorScale}></ColorLegend>
        </Card>}
      </div>
    );
  };
}

export default App;
