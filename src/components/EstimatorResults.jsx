import React from 'react';
import '../App.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {styles} from './styles';

class EstimatorResults extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <Card className={classes.root}>
                <CardContent>
                    <Typography variant="h3" gutterBottom>
                        Estimator results
                    </Typography>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Estimated GCC
                    </Typography>
                    <Typography variant="h4" gutterBottom>
                        {this.props.estimatedGcc}
                    </Typography>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Run time
                    </Typography>
                    <Typography variant="h4" gutterBottom>
                        {this.props.runTimeMs}Ms
                    </Typography>
                </CardContent>
            </Card>
        );
    };
}

export default withStyles(styles)(EstimatorResults);
