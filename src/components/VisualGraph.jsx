import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

require("highcharts/modules/networkgraph")(Highcharts);

class VisualGraph extends React.Component {
    maxEdgesLimit = 1000;
    constructor(props) {
        super(props);

        this.state = {
            chartOptions: {
                chart: {
                    type: 'networkgraph',
                },
                title: {
                    text: 'Visual Network Graph'
                },
                subtitle: {
                    text: `The visualization is limited up to ${ this.maxEdgesLimit } edges`
                },
                plotOptions: {
                    networkgraph: {
                        keys: ['from', 'to'],
                        layoutAlgorithm: {
                            approximation: 'barnes-hut',
                            enableSimulation: true,
                            friction: -0.9
                        },
                        color: this.props.color
                    }
                },
                series: [{
                    dataLabels: {
                        enabled: true,
                        linkFormat: ''
                    },
                    id: 'lang-tree',
                    data: this.sliceData()
                }]
            }
        };
    }
    componentDidUpdate(prevProps) {
        if(prevProps.data !== this.props.data ||
            prevProps.color !== this.props.color) {
            this.updateData();
        }

    }

    updateData() {
        const data = this.sliceData();
        this.setState({
            chartOptions: {
                series: [{data}],
                plotOptions: {networkgraph: {color: this.props.color}}
            }
        });
    }

    sliceData() {
        let data = this.props.data;
        if(data.length > this.maxEdgesLimit) {
            data = data.slice(0, this.maxEdgesLimit);
        }
        return data;
    }

    render() {
        return (
            <div>
                < HighchartsReact highcharts={Highcharts}
                    options={this.state.chartOptions} />
            </div>
        );
    }
}
export default VisualGraph;